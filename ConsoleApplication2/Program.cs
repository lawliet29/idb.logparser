﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing.Reader;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    class Program
    {
        private const string folderName = "LogFiles";
        static void Main(string[] args)
        {
            var logs = new List<LogLine>();
            foreach (var filePath in Directory.GetFiles(folderName))
            {
                logs.AddRange(File.ReadLines(filePath).Select(logLine => new LogLine(logLine)));
            }
            var sessions =
                logs.GroupBy(l => l.MemberEmail)
                    .Select(gr => new {Email = gr.Key, Steps = gr.OrderBy(l => l.Date)})
                    .ToList();

            var finishedCount = sessions.Count(s => s.Steps.Last().LogEventType == LogEventType.Finished);
            var timeoutedCount = sessions.Count(s => s.Steps.Last().LogEventType == LogEventType.Timeouted);
            var onlyStartedCount = sessions.Count(s => s.Steps.Last().LogEventType == LogEventType.Started);
            var onlyChainIdCount = sessions.Count(s => s.Steps.Last().LogEventType == LogEventType.ChainId);
            var failedCount = (sessions.Count - finishedCount - timeoutedCount);

            var stoppedOnChallenge = sessions.Where(s => s.Steps.Last().LogEventType != LogEventType.Finished && s.Steps.Any(ss => ss.LogEventType == LogEventType.Challenge)).ToList();
            var stoppedOnFaceCaptureCount = stoppedOnChallenge.Count(s => s.Steps.Last(ss => ss.LogEventType == LogEventType.Challenge).ChallengeType == ChallengeTypeEnum.FaceCapture);
            var stoppedOnDriversLicenseCaptureFrontCount = stoppedOnChallenge.Count(s => s.Steps.Last(ss => ss.LogEventType == LogEventType.Challenge).ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFront);
            var stoppedOnDriversLicenseCaptureFrontTextCount = stoppedOnChallenge.Count(s => s.Steps.Last(ss => ss.LogEventType == LogEventType.Challenge).ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFrontText);

            Console.WriteLine("Sessions started \t\t{0}", sessions.Count);
            Console.WriteLine("Sessions finished: \t\t{0} ({1:0.0}%)", finishedCount, finishedCount * 100 / sessions.Count);
            Console.WriteLine("Sessions timeouted: \t\t{0} ({1:0.0}%)", timeoutedCount, timeoutedCount * 100 / sessions.Count);
            Console.WriteLine("Sessions failed: \t\t{0} ({1:0.0}%)", failedCount, failedCount * 100 / sessions.Count);
            Console.WriteLine("=====================================");
            Console.WriteLine("{0} session left in started state", onlyStartedCount);
            Console.WriteLine("{0} sessions left with chainId", onlyChainIdCount);
            Console.WriteLine("{0} sessions stopped on FaceCapture", stoppedOnFaceCaptureCount);
            Console.WriteLine("{0} sessions stopped on DriversLicenseCaptureFront", stoppedOnDriversLicenseCaptureFrontCount);
            Console.WriteLine("{0} sessions stopped on DriversLicenseCaptureFrontText", stoppedOnDriversLicenseCaptureFrontTextCount);
            Console.WriteLine("=====================================");
            Console.WriteLine("Average response times:");

            var gotChainId = sessions.Where(s => s.Steps.Any(ss => ss.ChallengeType == ChallengeTypeEnum.FaceCapture) && s.Steps.Any(ss => ss.LogEventType == LogEventType.ChainId)).ToList();
            var averageChainIdGettingTime =
                gotChainId.Average(
                    s =>
                        (s.Steps.First(ss => ss.ChallengeType == ChallengeTypeEnum.FaceCapture).Date -
                         s.Steps.First(ss => ss.LogEventType == LogEventType.ChainId).Date).TotalMilliseconds);

            Console.WriteLine("ChainId:\t\t\t {0} ms", averageChainIdGettingTime);

            var gotFaceCapture = sessions.Where(s => s.Steps.Any(ss => ss.ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFront)).ToList();
            var averageFaceCaptureGettingTime =
                gotFaceCapture.Average(
                    s =>
                        (s.Steps.First(ss => ss.ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFront).Date -
                         s.Steps.First(ss => ss.ChallengeType == ChallengeTypeEnum.FaceCapture).Date).TotalMilliseconds);

            Console.WriteLine("FaceCapture:\t\t\t {0} ms", averageFaceCaptureGettingTime);

            var gotDriversLicenseCaptureFront = sessions.Where(s => s.Steps.Any(ss => ss.ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFrontText)).ToList();
            var averageDriversLicenseCaptureFrontGettingTime =
                gotDriversLicenseCaptureFront.Average(
                    s =>
                        (s.Steps.First(ss => ss.ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFrontText).Date -
                         s.Steps.First(ss => ss.ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFront).Date).TotalMilliseconds);

            Console.WriteLine("DriversLicenseCaptureFront:\t\t {0} ms", averageDriversLicenseCaptureFrontGettingTime);

            var logEnd = sessions.SelectMany(s => s.Steps).Max(step => step.Date);
            var averageDriversLicenseCaptureFrontTextGettingTime =
                gotDriversLicenseCaptureFront.Average(
                    s =>
                        (
                        (s.Steps.Any(ss => ss.LogEventType == LogEventType.Finished || ss.LogEventType == LogEventType.Timeouted)
                        ? s.Steps.First(ss => ss.LogEventType == LogEventType.Finished || ss.LogEventType == LogEventType.Timeouted).Date
                        : logEnd) -
                         s.Steps.First(ss => ss.ChallengeType == ChallengeTypeEnum.DriversLicenseCaptureFrontText).Date)
                         .TotalMilliseconds);

            Console.WriteLine("DriversLicenseCaptureFront:\t\t {0} ms", averageDriversLicenseCaptureFrontTextGettingTime);
            Console.ReadLine();
        }
    }
}
