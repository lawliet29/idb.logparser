﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public class LogLine
    {
        private const char Delemiter = '\t';

        private static readonly string[] LogEventTypeStrings = Enum.GetNames(typeof (LogEventType));

        public DateTime Date { get; private set; }
        public LogType LogType { get; private set; }
        public string MemberEmail { get; private set; }
        public LogEventType LogEventType { get; private set; }
        public ChallengeTypeEnum ChallengeType { get; private set; }

        public LogLine(string log)
        {
            log = log.TrimEnd('\u0004');
            var parts = log.Split(Delemiter);
            Date = DateTime.Parse(parts[0]);
            
            LogType type;
            if(Enum.TryParse(parts[1], true, out type))
                LogType = type;
            else
                throw new Exception("Incorrect log type");

            MemberEmail = parts[2];

            LogEventType eventType;
            if (Enum.TryParse(parts[3], true, out eventType))
                LogEventType = eventType;
            else
            {
                var typeString =
                    LogEventTypeStrings.FirstOrDefault(
                        ts => parts[3].StartsWith(ts, true, CultureInfo.InvariantCulture));
                if(typeString == null)
                    throw new Exception("Incorrect log event type");
                LogEventType = (LogEventType)Enum.Parse(typeof(LogEventType), typeString);
            }

            if (LogEventType == LogEventType.Challenge)
                ChallengeType = (ChallengeTypeEnum)Enum.Parse(typeof(ChallengeTypeEnum), parts[4]);

        }
    }
}
