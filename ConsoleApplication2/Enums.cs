﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication2
{
    public enum LogEventType
    {
        Started,
        ChainId,
        Challenge,
        Finished,
        Timeouted
    }

    public enum LogType
    {
        Trace,
        Error
    }

    public enum ChallengeTypeEnum
    {
        None,
        EmailCapture,
        EmailVerification,
        PhoneNumberCapture,
        PhoneNumberVerification,
        FaceCapture,
        PassportCapture,
        DriversLicenseCaptureFront,
        DriversLicenseCaptureFrontText,
        DriversLicenseCaptureBack,
        DeviceRegistration,
    }
}
